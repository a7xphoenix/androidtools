package com.a7xphoenix.tools;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

public class myView extends LinearLayout {

    public myView(Context context) {
        super(context);
        initialize(context);
    }

    public myView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    private void initialize(Context context){
        inflate(context, R.layout.myview, this);
    }
}
