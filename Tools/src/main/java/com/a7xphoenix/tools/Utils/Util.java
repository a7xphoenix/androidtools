package com.a7xphoenix.tools.Utils;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amulyakhare.textdrawable.TextDrawable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;


public class Util {

    public static class Strings {
        public static boolean isValidEmail(CharSequence target) {
            return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }

        public static String withCommaAsString(String value) {
            return "\"" + value + "\"";
        }

        public static List<String> withCommaAsList(List<String> stringList) {
            List<String> strings = new ArrayList();
            Iterator var2 = stringList.iterator();

            while(var2.hasNext()) {
                String value = (String)var2.next();
                strings.add(withCommaAsString(value));
            }

            return strings;
        }

        public static List<String> separatorReplace(String separatorColumnSign, String separatorLineSign, List<String> stringList) {
            List<String> strings = new ArrayList();
            Iterator var4 = stringList.iterator();

            while(var4.hasNext()) {
                String value = (String)var4.next();
                strings.add(value.replace(separatorColumnSign, ",").replace(separatorLineSign, "\r\n"));
            }

            return strings;
        }

    }

    public static class Randoms {
        public static int GetInteger(int max){
            Random random = new Random();
            return random.nextInt(max);
        }

        public static int GetInteger(int between, int and){
            int max, min;
            if (between > and){
                max = between;
                min = and;
            } else {
                max = and;
                min = between;
            }
            Random random = new Random();
            return random.nextInt((max - min) + 1) + min;
        }

        public static boolean GetBool(){
            Random random = new Random();
            return random.nextBoolean();
        }

        public static String FromCharacters(int cant, @Nullable String alphabet){
            String _alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (alphabet != null)
                _alphabet = alphabet;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < cant; i++) {
                sb.append(_alphabet.charAt(Util.Randoms.GetInteger(_alphabet.length())));
            }
            return sb.toString();
        }

        public static int RandomColor(){
            ArrayList<Integer> colores = new ArrayList<Integer>();
            colores.add(Color.BLUE);
            colores.add(Color.BLACK);
            colores.add(Color.CYAN);
            colores.add(Color.DKGRAY);
            colores.add(Color.GRAY);
            colores.add(Color.GREEN);
            colores.add(Color.WHITE);
            colores.add(Color.YELLOW);
            colores.add(Color.MAGENTA);

            return colores.get(Randoms.GetInteger(colores.size()));
        }

        public static String RandomOf(ArrayList<String> array){
            return array.get(Randoms.GetInteger(array.size()));
        }

        public static String RandomOf(List<String> array){
            return array.get(Randoms.GetInteger(array.size()));
        }

        public static String RandomString(List<String> array){
            return array.get(Randoms.GetInteger(array.size() - 1));
        }
    }

    public static class Dates {
        public static String CERO = "0";
        public static String BARRA = "-";

        public static String ToString(Calendar date){
            return ToString(date, null);
        }

        public static String ToString(Calendar date, @Nullable String separador){
            String barra = separador == null ? BARRA : separador;
            int dia = date.get(Calendar.DAY_OF_MONTH);
            int mes = date.get(Calendar.MONTH);
            int anio = date.get(Calendar.YEAR);
            String diaFormateado = (dia < 10) ? CERO + String.valueOf(dia) : String.valueOf(dia);
            String mesFormateado = (mes+1 < 10) ? CERO + String.valueOf(mes) : String.valueOf(mes);
            return diaFormateado + barra + mesFormateado + barra + anio;
        }

        public static String ToAPIString(Calendar date){
            int dia = date.get(Calendar.DAY_OF_MONTH);
            int mes = date.get(Calendar.MONTH)+1;
            int anio = date.get(Calendar.YEAR);
            String diaFormateado = (dia < 10) ? CERO + String.valueOf(dia) : String.valueOf(dia);
            String mesFormateado = (mes < 10) ? CERO + String.valueOf(mes) : String.valueOf(mes);
            return anio + BARRA + mesFormateado + BARRA + diaFormateado;
        }

    }

    public static class Draw {
        public static class FromText {
            public static TextDrawable generateDrawable(String text, int fontsize) {
                return TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.BLACK)
                        .useFont(Typeface.DEFAULT)
                        .fontSize(fontsize)
                        .bold()
                        .toUpperCase()
                        .endConfig()
                        .buildRect(text, Color.TRANSPARENT);
            }
        }
    }

    public static class Progress {

        public static ProgressDialog Show(Context context, String message) {
            return Show(context, "", message);
        }

        public static ProgressDialog Show(Context context, String title, String message) {
            return Show(context, title, message, true);
        }

        public static ProgressDialog Show(Context context, String title, String message, boolean indeterminate) {
            return ProgressDialog.show(context, title, message, indeterminate);
        }
    }

    public static class FCM {
        private static void suscribe(String topic, final String msg_success){
            FirebaseMessaging.getInstance().subscribeToTopic(topic)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!msg_success.isEmpty())
                                Toast.makeText(App.getContext(), msg_success, Toast.LENGTH_SHORT).show();
                        }
                    });
        }

        private static void unsuscribe(String topic, final String msg_success){
            FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!msg_success.isEmpty())
                                Toast.makeText(App.getContext(), msg_success, Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    public static void TakeAndShareSS(final Activity activity, final String message){
        Dexter.withActivity(activity)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {

                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {// check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            ScreenShot.getInstance().buildShotAndShare(activity, message);
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

}
