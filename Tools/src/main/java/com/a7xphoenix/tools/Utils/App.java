package com.a7xphoenix.tools.Utils;

import android.app.Activity;

public class App {
    private static final android.app.Application APP;

    public static android.content.Context getContext(){
        return APP.getApplicationContext();
    }

    static {
        try{
            Class<?> c = Class.forName("android.app.Activity");
            APP = (android.app.Application) c.getDeclaredMethod("currentApplication").invoke(null);
        } catch (Throwable ex){
            throw new AssertionError(ex);
        }
    }

    public static class Environment {
        public static boolean offline_mode = false;
    }
}
