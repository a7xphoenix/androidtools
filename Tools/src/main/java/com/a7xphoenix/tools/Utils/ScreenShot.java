package com.a7xphoenix.tools.Utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ScreenShot {
    private static final ScreenShot ourInstance = new ScreenShot();
    private boolean isActive = false;
    private Handler mhandler;
    private ScreenShot.TakeShot takeShot;

    public static ScreenShot getInstance() {
        return ourInstance;
    }

    private ScreenShot() {
    }

    public Bitmap buildShot(Activity activity) {
        View v = activity.getWindow().getDecorView().getRootView();
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache(true);
        Bitmap b = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        return b;
    }

    public Uri saveShot(Context context, Bitmap image, String filename) {
        File bitmapFile = this.getOutputMediaFile(filename);
        if (bitmapFile == null) {
            Log.d("ContentValues", "Error creating media file, check storage permissions: ");
            return null;
        } else {
            try {
                FileOutputStream fos = new FileOutputStream(bitmapFile);
                image.compress(CompressFormat.PNG, 90, fos);
                fos.close();
                MediaScannerConnection.scanFile(context, new String[]{bitmapFile.getPath()}, new String[]{"image/jpeg"}, (OnScanCompletedListener)null);
                return FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", bitmapFile);
                //return Uri.fromFile(bitmapFile);
            } catch (FileNotFoundException var6) {
                Log.d("ContentValues", "File not found: " + var6.getMessage());
                return null;
            } catch (IOException var7) {
                Log.d("ContentValues", "Error accessing file: " + var7.getMessage());
                return null;
            }
        }
    }

    private File getOutputMediaFile(String filename) {
        File mediaStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator);
        if (!mediaStorageDirectory.exists() && !mediaStorageDirectory.mkdirs()) {
            return null;
        } else {
            String timeStamp = (new SimpleDateFormat("ddMMyyyy_HHmmss")).format(new Date());
            String mImageName = filename + timeStamp + ".jpg";
            File mediaFile = new File(mediaStorageDirectory.getPath() + File.separator + mImageName);
            return mediaFile;
        }
    }

    public void buildContinousShot(Activity activity, long millisecond) {
        this.isActive = true;
        this.mhandler = new Handler();
        this.takeShot = new ScreenShot.TakeShot(activity, millisecond);
        this.mhandler.postDelayed(this.takeShot, millisecond);
    }

    public void stopContinousShot() {
        if (this.isActive) {
            this.isActive = false;
            this.mhandler.removeCallbacks(this.takeShot);
        }

    }

    public void buildShotAndShare(Activity activity) {
        Bitmap bitmap = this.buildShot(activity);
        Uri uri = null;

        try {
            uri = this.saveShot(activity, bitmap, "screenshot");
        } catch (NullPointerException var5) {
            Log.e("error", "null uri for file");
            return;
        }

        this.shareShot("Choose an app", activity, uri);
    }

    private void shareShot(String s, Activity activity, Uri uri) {
        Intent shareIntent = new Intent();
        shareIntent.setAction("android.intent.action.SEND");
        shareIntent.putExtra("android.intent.extra.STREAM", uri);
        shareIntent.setType("image/*");

        try {
            activity.startActivity(Intent.createChooser(shareIntent, "Choose an app"));
        } catch (ActivityNotFoundException var6) {
            Toast.makeText(activity, "There is no app that can handle sharing", Toast.LENGTH_SHORT).show();
        }

    }

    public void buildShotAndShare(Activity activity, String shareMsg) {
        Bitmap bitmap = this.buildShot(activity);
        Uri uri = null;

        try {
            uri = this.saveShot(activity, bitmap, "screenshot");
        } catch (NullPointerException var6) {
            Log.e("error", "null uri for file");
            return;
        }

        this.shareShot("Choose an app", activity, uri, shareMsg);
    }

    private void shareShot(String s, Activity activity, Uri uri, String message) {
        Intent shareIntent = new Intent();
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.setAction("android.intent.action.SEND");
        shareIntent.putExtra("android.intent.extra.STREAM", uri);
        shareIntent.putExtra("android.intent.extra.TEXT", message);
        shareIntent.setType("image/*");

        try {
            activity.startActivity(Intent.createChooser(shareIntent, "Choose an app"));
        } catch (ActivityNotFoundException var7) {
            Toast.makeText(activity, "There is no app that can handle sharing", Toast.LENGTH_SHORT).show();
        }

    }

    class TakeShot implements Runnable {
        private final Activity activity;
        private long millisec;

        TakeShot(Activity activity, long millisec) {
            this.activity = activity;
            this.millisec = millisec;
        }

        public void run() {
            if (ScreenShot.this.isActive) {
                Bitmap bitmap = ScreenShot.this.buildShot(this.activity);
                ScreenShot.this.saveShot(this.activity, bitmap, "shot");
                ScreenShot.this.mhandler.postDelayed(ScreenShot.this.takeShot, this.millisec);
            }

        }
    }

}
