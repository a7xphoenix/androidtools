package com.a7xphoenix.tools.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

public class BaseAdapter2<T> extends BaseAdapter {
    protected Context context;
    protected int layout;
    protected ArrayList<T> array;
    protected ArrayList<T> copy = new ArrayList<T>();

    public BaseAdapter2(Context context, int layout, ArrayList<T> array){
        this.context = context;
        this.layout = layout;
        this.array = array;
    }

    @Override
    public int getCount() {
        return this.array.size();
    }

    @Override
    public Object getItem(int position) {
        return this.array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}

