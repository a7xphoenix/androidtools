package com.a7xphoenix.tools.Fragments;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;


public class Fragment2 extends Fragment {
    ProgressDialog dialog;
    protected View view;

    protected FragmentActivity myContext;

    public Fragment2(){
    }

    @Override
    public void onAttach(@NonNull Context context) {
        myContext = (FragmentActivity) context;
        super.onAttach(context);
    }
}
