package com.a7xphoenix.tools.Http;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApiMessage {
    @SerializedName("Message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
